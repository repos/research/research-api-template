import sys


def validate_project_slug() -> None:
    project_slug = "{{ cookiecutter.project_slug }}"
    if project_slug.islower() and project_slug.replace("-", "_").isidentifier():
        return

    print(
        f'"{project_slug}" is not a valid slug. '
        "A slug can only contain lowercase letters a-z, underscore (_), "
        "dash (-) and, except for the first character, the digits 0-9."
    )
    sys.exit(1)


def validate_project_import_name() -> None:
    project_import_name = "{{ cookiecutter.project_import_name }}"
    if project_import_name.islower() and project_import_name.isidentifier():
        return

    print(
        f'"{project_import_name}" is not a import name. '
        "A slug can only contain lowercase letters a-z, underscore (_) and, "
        "except for the first character, the digits 0-9."
    )
    sys.exit(1)


def main() -> None:
    validate_project_slug()
    validate_project_import_name()


if __name__ == "__main__":
    main()
