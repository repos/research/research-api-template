import re

import cookiecutter.exceptions  # type: ignore[import-untyped]
import pytest

from pytest_cookies.plugin import Cookies  # type: ignore[import-untyped]


@pytest.fixture
def context() -> dict[str, str]:
    return {
        "project_name": "Test API",
        "description": "This a test API.",
        "author_name": "Foo Bar",
        "author_email": "foo.bar@example.org",
    }


def test_generate_project(cookies: Cookies, context: dict[str, str]) -> None:
    result = cookies.bake(extra_context=context)

    assert result.exit_code == 0
    assert result.exception is None

    assert result.context["project_slug"] == "test-api"
    assert result.context["project_import_name"] == "test_api"
    assert result.project_path.name == "test-api"
    assert result.project_path.is_dir()

    # Check that placeholders have been subsituted across all files
    placeholder_re = re.compile(r"{{(\s?cookiecutter)[.](.*?)}}")
    for path in result.project_path.glob("**/*"):
        if path.is_file():
            assert placeholder_re.search(path.read_text()) is None


@pytest.mark.parametrize("slug", ("test api", "Test-Api", "test.api", "1test-api"))
def test_generate_project_invalid_slug(
    cookies: Cookies, context: dict[str, str], slug: str
) -> None:
    result = cookies.bake(extra_context=context | {"project_slug": slug})

    assert result.exit_code != 0
    assert isinstance(result.exception, cookiecutter.exceptions.FailedHookException)


@pytest.mark.parametrize(
    "import_name", ("test-api", "test api", "Test-Api", "test.api", "1test-api")
)
def test_generate_project_invalid_import_name(
    cookies: Cookies, context: dict[str, str], import_name: str
) -> None:
    result = cookies.bake(extra_context=context | {"project_import_name": import_name})

    assert result.exit_code != 0
    assert isinstance(result.exception, cookiecutter.exceptions.FailedHookException)
