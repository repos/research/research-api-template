# Research API Template

## About

A [cookiecutter](https://github.com/cookiecutter/cookiecutter) template for generating production-ready Research API boilerplate, instantly.

The generated project is a [FastAPI](https://fastapi.tiangolo.com) based service that:

- Works with Python 3.11.
- Can be easily deployed to [Cloud VPS](https://wikitech.wikimedia.org/wiki/Help:Cloud_VPS) or [Toolforge](https://wikitech.wikimedia.org/wiki/Help:Toolforge).
- Comes with Gitlab CI set up which also facilitates publishing production images to [Wikimedia docker registry](https://docker-registry.wikimedia.org).
- Runs tests using [pytest](https://docs.pytest.org).
- Has strict typechecking enabled using [mypy](https://mypy.readthedocs.io).
- Is integrated with [pre-commit](https://pre-commit.com) for identifying issues on every commit.
- Is easy to configure using environment variables or a `.env` file.

## Prerequisites

> **TIP**: This section uses `pipx` to install python command-line tools. `pipx` works differently from `pip` in that it installs applications in isolated environments, i.e. a new virtual environment for each tool installed, but still makes them globally available by symlinking them from `~/.local/bin`. You can install `pipx` by following the instructions [here](https://pipx.pypa.io/stable/installation/).

To use this template, you'll first need to install cookiecutter:

```bash
$ pipx install cookiecutter
```

## Usage

First, `cd` to the location where you want the code for your project to live. Then, to generate a new project using this template, run:

```bash
$ cookiecutter https://gitlab.wikimedia.org/repos/research/research-api-template.git
```

You'll be prompted for some values that will be used to customize the project for you. For most projects, you'd only want to change the project name and description and keep the defaults for the rest. For example, here's how you would answer the prompts if you were creating an API for serving the article quality model:

```
  [1/7] project_name (Research API): Article Quality Model
  [2/7] project_slug (article-quality-model):
  [3/7] project_import_name (article_quality_model):
  [4/7] description (API for an awesome Research project.): API for predicting the quality of a Wikipedia article.
  [5/7] author_name (Research Team):
  [6/7] author_email (research-wmf@lists.wikimedia.org):
  [7/7] version (0.1.0):
```

Once your project has been generated, it will have the following directory structure:

```
<project_slug>
├── .gitignore
├── .gitlab-ci.yml
├── .pipeline
│   └── blubber.yaml          -> Defines development and production docker
│                                images.
├── .pre-commit-config.yaml
├── LICENSE
├── Procfile                  -> Declares which command Toolforge should
│                                run to start the app.
├── README.md                 -> Contains instructions on how to develop,
│                                test and deploy your API.
├── poetry.lock               -> Lockfile containing all dependencies and
│                                their exact versions for reproducibility.
├── pyproject.toml            -> Contains project metadata, build system
│                                requirements and configuration for tools
│                                like linters and formatters.
├── requirements.txt          -> This file is required because the buildp-
│                                ack used by Toolforge does not currently
│                                support PEP 517 builds via `pyproject.toml`.
├── <project_import_name>     -> Subdirectory containing the code for the API.
│    ├── __init__.py
│    ├── app.py               -> Defines a factory function for instantiat-
│    │                           ing a `FastAPI` instance given a `Config`
│    │                           object.
│    ├── config.py            -> Defines the `Config` class which represents
│    │                           configurable properties of the app like log-
│    │                           ging level etc.
│    ├── main.py              -> Entry point for the app.
│    ├── routes               -> Subdirectory containing all routers config-
│    │   │                       ured by this API.
│    │   ├── __init__.py      -> Registers all subrouters and assigns prefixes.
│    │   ├── greeting.py      -> Subrouter for all endpoints related to "greet-
│    │   │                       ing" (for demonstration purposes only).
│    │   └── health.py        -> Subrouter defining the health check endpoint.
│    └── schemas.py           -> Declares Pydantic models for request and
│                                response bodies.
└── tests                     -> Subdirectory containing tests for the API.
    ├── __init__.py
    ├── conftest.py           -> Defines fixtures to be used by tests.
    └── routes
        ├── __init__.py
        ├── test_greeting.py
        └── test_health.py
```

To start developing, follow the instructions in the generated README.

To create a git repo for your project, run:

```bash
$ cd <your-project>
$ git init
$ git checkout -b main
$ git add .
$ git commit -m 'initial commit'
```

When you want to push your repo to Gitlab, [create a blank project](https://gitlab.wikimedia.org/projects/new#blank_project) and then:

```bash
$ git remote add origin <url-to-your-gitlab-repo>
$ git push -u origin main
```

## Bugs and feature requests

You can report bugs and open feature requests using the issue tracker.

When reporting bugs, please provide a short, self-contained and correct example.

When opening a feature request, please also describe the problem you intend to solve with the feature.
