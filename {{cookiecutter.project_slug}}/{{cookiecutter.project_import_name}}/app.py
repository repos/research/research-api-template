import logging

import fastapi

from fastapi.middleware.cors import CORSMiddleware

from {{ cookiecutter.project_import_name }}.config import Config, get_config
from {{ cookiecutter.project_import_name }}.routes import router as api_router


def create_app(config: Config) -> fastapi.FastAPI:
    app = fastapi.FastAPI()

    # Configure logging
    logging.basicConfig(level=config.log_level)
    logging.getLogger(name="uvicorn.access").setLevel(config.log_level)
    logging.getLogger(name="uvicorn.error").setLevel(config.log_level)

    # Enable cross-origin requests
    allowed_origins = ("http://localhost",)
    app.add_middleware(
        CORSMiddleware,
        allow_origins=allowed_origins,
        allow_credentials=True,
        allow_methods=("*",),
        allow_headers=("*",),
    )

    # Override config with the one passed in. This function asks
    # for `config` despite it being a dependency for routes as it makes
    # it simpler to switch it out for tests etc. It also makes this dependency
    # explicit which slightly minimizes the spooky action at a distance feel
    # of the fastapi dependency injection system.
    app.dependency_overrides[get_config] = lambda: config

    # Register routes
    app.include_router(api_router, prefix=config.api_prefix)

    return app
