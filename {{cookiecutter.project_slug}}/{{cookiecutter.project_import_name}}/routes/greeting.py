import datetime

from typing import Annotated, Final

import fastapi

from {{ cookiecutter.project_import_name }}.config import Config, get_config
from {{ cookiecutter.project_import_name }}.schemas import Greeting

router: Final = fastapi.APIRouter()


@router.get("/")
async def get_greeting(
    config: Annotated[Config, fastapi.Depends(get_config)],
    name: Annotated[str, fastapi.Query(min_length=2, max_length=50)] = "User",
) -> Greeting:
    """Get a greeting message addressed to `name`."""

    return Greeting(
        msg=f"{config.greeting}, {name}",
        time=datetime.datetime.now(),
    )
