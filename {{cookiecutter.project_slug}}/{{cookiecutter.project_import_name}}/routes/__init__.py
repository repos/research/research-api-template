from typing import Final

import fastapi

from {{ cookiecutter.project_import_name }}.routes import greeting, health

router: Final = fastapi.APIRouter()

# Register all defined routes
router.include_router(health.router, prefix="/health")
# This is an example sub-router, containing all routes related to "greeting".
router.include_router(greeting.router, prefix="/greeting")
