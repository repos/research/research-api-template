from typing import Final

import fastapi

router: Final = fastapi.APIRouter()


@router.get("/")
async def get_health() -> fastapi.Response:
    return fastapi.Response(status_code=fastapi.status.HTTP_200_OK)
