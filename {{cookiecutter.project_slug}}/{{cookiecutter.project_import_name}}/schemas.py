import datetime

import pydantic as pyd

from pydantic.dataclasses import dataclass as pyd_dataclass


@pyd_dataclass(frozen=True)
class Greeting:
    """Customized greeting for visitors."""

    msg: str = pyd.Field(description="Customised greeting message for user")
    time: datetime.datetime = pyd.Field(description="Time of the user's visit")
