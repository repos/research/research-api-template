import fastapi

from {{ cookiecutter.project_import_name }}.app import create_app
from {{ cookiecutter.project_import_name }}.config import Config, get_config

config: Config = get_config()
app: fastapi.FastAPI = create_app(config=config)
