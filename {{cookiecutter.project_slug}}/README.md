# {{ cookiecutter.project_name }}

## About

{{ cookiecutter.description }}

## Table of contents

- [Development](#development)
  - [Prerequisites](#prerequisites)
  - [Setup](#setup)
  - [Configuration](#configuration)
  - [Typechecking](#typechecking)
  - [Testing](#testing)
  - [Hooks](#hooks)
- [Deployment](#deployment)
  - [Cloud VPS](#cloud-vps)
  - [Toolforge](#toolforge)

## Development

### Prerequisites

- Install the latest version of [Poetry](https://python-poetry.org).

### Setup

Install the project in editable mode i.e. the default for `poetry`:

```bash
$ poetry install --no-root
```

> **NOTE**: If you're developing on the stat machines, the python version that comes with `conda-analytics` might be older than 3.11. If that's the case, create a conda env with python 3.11 and get its path:
>
> ```
> $ conda create --name py311 python=3.11
> $ conda env list | grep py311
> ```
>
> Let's say your path was `/srv/home/me/.conda/envs/py311`. You can tell poetry to use the python interpreter from this environment by running:
>
> ```
> $ poetry env use /srv/home/me/.conda/envs/py311/bin/python3.11
> ```
>
> Now run the installation command above and poetry should install the dependencies in a virtual environment for you.

Run the server, with hot reload enabled:

```bash
$ poetry run uvicorn {{ cookiecutter.project_import_name }}.main:app --reload
```

The API docs should be available at http://localhost:8000/docs.

### Configuration

The API will run with the default configuration in `{{ cookiecutter.project_import_name }}/config.py` which also contains information about each setting. You can override the defaults in 3 ways:

1. Pass a customized config object to the `create_app` factory function i.e. `create_app(config=Config(api_prefix="/api/v1"))`.
2. Set an environment variable i.e. `API_PREFIX=/api/v1`.
3. Create a `.env` file and add your environment variables to it.

The above are listed in order of priority which is used to determine the value of a setting if its value is specified using more than one ways.

### Typechecking

You can make sure your code typechecks using `mypy`:

```bash
$ poetry run mypy .
```

### Testing

To run all tests:

```bash
$ poetry run pytest -vv
```

You can choose which tests to run using the `-k test_name` option with `pytest`.

### Hooks

To set up pre-commit hooks, run:

```bash
$ poetry run pre-commit install
```

This will lint and format the code on every commit. By default, it will only run on changed files, but you can run it on all files manually:

```bash
$ poetry run pre-commit run --all-files
```

Tests, typechecking and hooks are also run in the CI.

## Deployment

### Cloud VPS

To deploy on [cloud VPS](https://wikitech.wikimedia.org/wiki/Help:Cloud_VPS), you'll first need to [install docker](https://docs.docker.com/engine/install/). Once `docker` is installed, build the production image:

```bash
$ docker build \
    --file .pipeline/blubber.yaml  \
    --target production \
    --tag production .
```

> **NOTE**: The CI pipeline has a `publish-image` job that automatically builds and publishes the `production` image to the [Wikimedia docker registry](https://docker-registry.wikimedia.org) each time a new tag is pushed and the job is running on a protected branch but this job can only run on trusted runners. Once access to trusted runners for this project has been requested by filing a phab task as per [instructions](https://wikitech.wikimedia.org/wiki/GitLab/Gitlab_Runner/Trusted_Runners), you'll no longer need to build this image yourself as described above.

Then run a docker container using the newly created image:

```bash
$ docker run \
    --detach \
    --publish 8080:80 \
    --env SERVER_WORKERS=4 \
    production
```

You can also set constraints on resources such as memory and cpu by passing `--memory=4g` and `--cpus=4`, for example, when invoking `docker run` (more details [here](https://docs.docker.com/config/containers/resource_constraints/)). To see the logs for this container, run:

```bash
$ docker logs <container-id>
```

When you want to make your service publicly accessible, you'll need to create a web proxy for your cloud VPS instance following the instructions [here](https://wikitech.wikimedia.org/wiki/Help:Using_a_web_proxy_to_reach_Cloud_VPS_servers_from_the_internet).

### Toolforge

To deploy on [Toolforge](https://wikitech.wikimedia.org/wiki/Help:Toolforge), first ssh into `login.toolforge.org`. Then run:

```bash
$ become {{ cookiecutter.project_slug }}
$ toolforge build start https://gitlab.wikimedia.org/repos/research/{{ cookiecutter.project_slug }}
```

Once the build finishes successfully, start the webservice:

```bash
$ toolforge webservice buildservice start --mount=none
```

See the [docs](https://wikitech.wikimedia.org/wiki/Help:Toolforge/Build_Service) for more details and troubleshooting information.
