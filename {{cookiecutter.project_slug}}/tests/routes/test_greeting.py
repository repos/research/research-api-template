import fastapi

from fastapi.testclient import TestClient as FastAPITestClient


def test_get_greeting(test_client: FastAPITestClient) -> None:
    response = test_client.get("/greeting", params={"name": "Foo"})
    assert response.status_code == fastapi.status.HTTP_200_OK

    data = response.json()
    assert data is not None
    assert data.get("msg") == "Test hello, Foo"
