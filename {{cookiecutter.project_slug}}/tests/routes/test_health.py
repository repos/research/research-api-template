import fastapi

from fastapi.testclient import TestClient as FastAPITestClient


def test_get_health(test_client: FastAPITestClient) -> None:
    response = test_client.get("/health")
    assert response.status_code == fastapi.status.HTTP_200_OK
